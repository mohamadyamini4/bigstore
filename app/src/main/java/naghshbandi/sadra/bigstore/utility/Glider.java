package naghshbandi.sadra.bigstore.utility;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.DataSource;
import com.bumptech.glide.load.engine.GlideException;
import com.bumptech.glide.request.RequestListener;
import com.bumptech.glide.request.target.SimpleTarget;
import com.bumptech.glide.request.target.Target;
import com.bumptech.glide.request.transition.Transition;

public class Glider {

    public static void gif(ImageView iv, int gifId) {
        Glide.with(iv.getContext().getApplicationContext())
                .asGif()
                .centerCrop()
                .load(gifId)
                .into(iv);
    }

    public static void load(int drawable, ImageView iv) {
        Glide.with(iv.getContext())
                .load(drawable)
                .into(iv);
    }

    public static void load(int drawable, ImageView iv, int size) {
        Glide.with(iv.getContext())
                .load(drawable)
                .override(size, size)
                .into(iv);
    }

    public static void load(String iPath, ImageView iv) {
        Glide.with(iv.getContext())
                .load(iPath)
                .into(iv);
    }

    public static void load(String iPath, ImageView iv, int drawable) {
        Glide.with(iv.getContext())
                .load(iPath)
                .error(drawable)
                .placeholder(drawable)
                .into(iv);
    }

    public static void load(String iPath, ImageView iv, int drawable, int size) {
        Glide.with(iv.getContext())
                .load(iPath)
                .override(size)
                .error(drawable)
                .placeholder(drawable)
                .into(iv);
    }

    public static void loadBanner(String iPath, ImageView iv, int defaultImage, ImageCallback callback) {
        Glide.with(iv.getContext())
                .load(iPath)
                .listener(getListener(callback))
                .error(defaultImage)
                .placeholder(defaultImage)
                .into(iv);
    }

    public static void loadWithCallback(String iPath, ImageView iv, ImageCallback callback) {
        Glide.with(iv.getContext())
                .load(iPath)
                .listener(getListener(callback))
                .into(iv);
    }

    public static void loadWithReSize(String iPath, ImageView iv, float size) {
//        Glide.with(iv.getContext())
//                .load(iPath)
//                .override(Util.dpToPx(size, iv), Util.dpToPx(size, iv))
//                .placeholder(R.drawable.iv)
//                .error(R.drawable.ic_account_dark)
//                .into(iv);
    }

    public static void loadImage(String iPath, ImageView iv, ImageCallback callback) {
        Glide.with(iv.getContext())
                .load(iPath)
                .dontAnimate()
                .listener(getListener(callback))
                .into(iv);
    }

//iv    public static void loadWithReSizeAndCallback(String iPath, RoundedImageView iv, float size, ImageCallback callback) {
////        GlideApp.with(iv.getContext())
////                .load(iPath)
////                .override(Util.dpToPx(size, iv), Util.dpToPx(size, iv))
////                .listener(getListener(callback))
////                .into(iv);
//    }

    public static void cache(Context c, String iPath, ImageCallback callback) {
        Context context = c.getApplicationContext();
        Glide.with(context)
                .asBitmap()
                .load(iPath)
                .addListener(getCacheListener(callback))
                .into(new SimpleTarget<Bitmap>() {
                    @Override
                    public void onResourceReady(@NonNull Bitmap resource, @Nullable Transition<? super Bitmap> transition) {

                    }
                });
    }

    private static RequestListener<Bitmap> getCacheListener(final ImageCallback callback) {
        return new RequestListener<Bitmap>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Bitmap> target, boolean isFirstResource) {
                callback.onFailure();
                return false;
            }

            @Override
            public boolean onResourceReady(Bitmap resource, Object model, Target<Bitmap> target, DataSource dataSource, boolean isFirstResource) {
                callback.onSuccess();
                return false;
            }
        };
    }

    private static RequestListener<Drawable> getListener(final ImageCallback callback) {
        return new RequestListener<Drawable>() {
            @Override
            public boolean onLoadFailed(@Nullable GlideException e, Object model, Target<Drawable> target, boolean isFirstResource) {
                callback.onFailure();
                return false;
            }

            @Override
            public boolean onResourceReady(Drawable resource, Object model, Target<Drawable> target, DataSource dataSource, boolean isFirstResource) {
                callback.onSuccess();
                return false;
            }
        };
    }

    public interface ImageCallback {
        void onSuccess();

        void onFailure();
    }
}

