package naghshbandi.sadra.bigstore.objects;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.ForeignKey;

@Entity(tableName = "t_product_user_join",
        primaryKeys = {"userId", "repoId"},
        foreignKeys = {@ForeignKey(entity = UserObj.class, parentColumns = "id", childColumns = "userId"),
                @ForeignKey(entity = ProductDbObj.class, parentColumns = "id", childColumns = "repoId")})
public class ProductUserJoin {
    public final int userId;
    public final int repoId;

    public ProductUserJoin(final int userId, final int repoId) {
        this.userId = userId;
        this.repoId = repoId;
    }
}
