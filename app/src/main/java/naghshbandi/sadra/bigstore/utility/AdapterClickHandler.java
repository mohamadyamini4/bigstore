package naghshbandi.sadra.bigstore.utility;

public interface AdapterClickHandler {
    void onClick(int position);
}
