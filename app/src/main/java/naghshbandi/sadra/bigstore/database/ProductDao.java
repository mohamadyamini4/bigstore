package naghshbandi.sadra.bigstore.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import naghshbandi.sadra.bigstore.objects.ProductDbObj;

@Dao
public interface ProductDao {

    @Insert
    long insertProduct(ProductDbObj productDbObj);

    @Query("SELECT * FROM t_product where id=:Id")
    ProductDbObj getProduct(int Id);

    @Query("SELECT * FROM t_product")
    List<ProductDbObj> getProducts();

}
