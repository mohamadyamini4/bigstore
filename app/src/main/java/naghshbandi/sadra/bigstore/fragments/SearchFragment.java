package naghshbandi.sadra.bigstore.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.adapters.ProductAdapter;
import naghshbandi.sadra.bigstore.network.ApiClient;
import naghshbandi.sadra.bigstore.network.ApiInterface;
import naghshbandi.sadra.bigstore.objects.ProductObj;
import naghshbandi.sadra.bigstore.utility.MyDividerItemDecoration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SearchFragment extends Fragment {

    private boolean hasLoadedOnce;
    private List<ProductObj> productObjs = new ArrayList<>();
    private List<ProductObj> productObjs2 = new ArrayList<>();
    private ProductAdapter productAdapter;
    private EditText edS;

    public SearchFragment() {
        // Required empty public constructor
    }


    public static SearchFragment newInstance() {
        return new SearchFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View inflate = inflater.inflate(R.layout.fragment_search, container, false);
        productAdapter = new ProductAdapter(productObjs, getActivity(), position -> {

        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        RecyclerView recyclerView = inflate.findViewById(R.id.recView);
        edS = inflate.findViewById(R.id.editText);
        edS.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (edS.getText().toString().trim().isEmpty()) {
                    productObjs.clear();
                    productAdapter.notifyDataSetChanged();
                    return;
                }
                productObjs.clear();
                for (ProductObj productObj : productObjs2) {
                    if (productObj.getTitle().contains(edS.getText().toString())) {
                        productObjs.add(productObj);
                    }
                }
                productAdapter.notifyDataSetChanged();
            }
        });
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL, 3));
        recyclerView.setAdapter(productAdapter);


        return inflate;

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(true);
        if (isVisibleToUser && !hasLoadedOnce) {
            setup();
            hasLoadedOnce = true;
        }
    }

    private void setup() {
        ApiInterface ai = ApiClient.getClient(getActivity()).create(ApiInterface.class);
        ai.getProducts().enqueue(new Callback<List<ProductObj>>() {
            @Override
            public void onResponse(Call<List<ProductObj>> call,
                                   Response<List<ProductObj>> response) {
                productObjs2.addAll(response.body());
            }

            @Override
            public void onFailure(Call<List<ProductObj>> call, Throwable t) {

            }
        });
    }

}
