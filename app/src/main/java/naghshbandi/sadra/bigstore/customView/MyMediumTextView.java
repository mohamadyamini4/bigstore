package naghshbandi.sadra.bigstore.customView;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;

public class MyMediumTextView extends android.support.v7.widget.AppCompatTextView {

    public MyMediumTextView(Context context) {
        super(context);
        init();
    }

    public MyMediumTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public MyMediumTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        Typeface tf = Typeface.createFromAsset(getContext().getAssets(), "IRANSans(FaNum).ttf");
        setTypeface(tf);

    }
}