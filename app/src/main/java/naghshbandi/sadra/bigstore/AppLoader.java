package naghshbandi.sadra.bigstore;

import android.app.Application;
import android.content.Context;

import com.crashlytics.android.Crashlytics;

import io.fabric.sdk.android.Fabric;

public class AppLoader extends Application {
    private static Context appContext;
    private static Application application;

    public static Application getApplication() {
        return application;
    }

    public static Context getAppContext() {
        return appContext;
    }

    @Override
    public void onCreate() {
        Fabric fabric = new Fabric.Builder(this)
                .kits(new Crashlytics())
                .debuggable(true)
                .build();
        Fabric.with(fabric);

        appContext = getApplicationContext();
        application = getApplication();
        super.onCreate();
    }
}
