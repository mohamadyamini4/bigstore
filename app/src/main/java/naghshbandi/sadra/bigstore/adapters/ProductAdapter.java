package naghshbandi.sadra.bigstore.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.objects.ProductObj;
import naghshbandi.sadra.bigstore.utility.AdapterClickHandler;
import naghshbandi.sadra.bigstore.utility.Glider;

public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.ViewHolder> {
    private List<ProductObj> productObjs;
    private Context context;
    private AdapterClickHandler adapterClickHandler;

    public ProductAdapter(List<ProductObj> productObjs, Context context, AdapterClickHandler adapterClickHandler) {
        this.productObjs = productObjs;
        this.context = context;
        this.adapterClickHandler = adapterClickHandler;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_product,
                viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        ProductObj productObj = productObjs.get(i);
        viewHolder.title.setText(productObj.getTitle());
        viewHolder.price.setText(String.format("%s تومان ", productObj.getPrice()));
        Glider.load(productObj.getThumbnail(), viewHolder.thumbnail);
    }

    @Override
    public int getItemCount() {
        return productObjs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, price;
        ImageView thumbnail;
        ConstraintLayout constraintLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvTitle);
            price = itemView.findViewById(R.id.tvPrice);
            thumbnail = itemView.findViewById(R.id.ivProduct);
            constraintLayout = itemView.findViewById(R.id.clContainer);
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapterClickHandler != null) {
                        adapterClickHandler.onClick(getAdapterPosition());
                    }
                }
            });
        }
    }
}
