package naghshbandi.sadra.bigstore.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import naghshbandi.sadra.bigstore.AppLoader;
import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.objects.ProductIndexObj;
import naghshbandi.sadra.bigstore.utility.AdapterClickHandler;

public class ProductIndexAdapter extends RecyclerView.Adapter<ProductIndexAdapter.ViewHolder> {
    private List<ProductIndexObj> productIndexObjs;
    private AdapterClickHandler adapterClickHandler;

    ProductIndexAdapter(List<ProductIndexObj> productIndexObjs) {
        this.productIndexObjs = productIndexObjs;
    }

    public AdapterClickHandler getAdapterClickHandler() {
        return adapterClickHandler;
    }

    public void setAdapterClickHandler(AdapterClickHandler adapterClickHandler) {
        this.adapterClickHandler = adapterClickHandler;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_index_product,
                viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder v, int i) {
        ProductIndexObj product = productIndexObjs.get(i);
        Glide.with(AppLoader.getAppContext())
                .load(product.getImage())
                .error(R.drawable.index_product)
                .into(v.image);
        v.price.setText(product.getPrice());
        v.title.setText(product.getTitle());
    }

    @Override
    public int getItemCount() {
        return productIndexObjs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title, price;
        ImageView image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvNameIndexProduct);
            price = itemView.findViewById(R.id.tvPriceIndexProduct);
            image = itemView.findViewById(R.id.ivThumbnailIndexProduct);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapterClickHandler != null)
                        adapterClickHandler.onClick(getAdapterPosition());
                }
            });
        }
    }
}
