package naghshbandi.sadra.bigstore.objects;

import com.google.gson.annotations.SerializedName;

public class ProductIndexObj {
    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title = "رضا صادقی - زندگی کن";
    @SerializedName("price")
    private String price = "۱۰۰۰ تومان";
    @SerializedName("image")
    private String image = "";
    @SerializedName("type")
    private ProductType productType = ProductType.MUSIC;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getImage() {
        return image;
    }
}
