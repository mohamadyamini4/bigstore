package naghshbandi.sadra.bigstore;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import naghshbandi.sadra.bigstore.adapters.ProductDaoAdapter;
import naghshbandi.sadra.bigstore.database.DatabaseHelper;
import naghshbandi.sadra.bigstore.objects.ProductDbObj;
import naghshbandi.sadra.bigstore.utility.MyDividerItemDecoration;
import naghshbandi.sadra.bigstore.utility.MyPreferences;

public class ProfileActivity extends AppCompatActivity {

    private ProductDaoAdapter productDaoAdapter;
    private List<ProductDbObj> productDbObjs = new ArrayList<>();
    private RecyclerView recyclerView;
    private TextView name;
    private TextView wallet;
    private TextView inc;
    private EditText editText;
    private String curWallet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        recyclerView = findViewById(R.id.recView);
        wallet = findViewById(R.id.textView4);
        inc = findViewById(R.id.inc);
        editText = findViewById(R.id.editText2);
        productDaoAdapter = new ProductDaoAdapter(productDbObjs, this, position -> {
            Intent i = new Intent(this, eachProductActivity.class);
            i.putExtra("id", productDbObjs.get(position).getId());
            startActivity(i);
        });
        curWallet = MyPreferences.getString(MyPreferences.PREF_WALLET, "0");
        wallet.setText(String.format("موجودی  %s تومان ", curWallet));
        inc.setOnClickListener(v -> {
            String s = editText.getText().toString();
            if (s.length() < 1)
                return;
            curWallet = String.valueOf(Integer.valueOf(curWallet) + Integer.valueOf(s));
            MyPreferences.saveString(MyPreferences.PREF_WALLET, curWallet);
            wallet.setText(String.format("موجودی  %s تومان ", curWallet));
            Toast.makeText(this, "افزایش موجودی با موفقیت انجام شد", Toast.LENGTH_SHORT).show();

        });

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this,
                DividerItemDecoration.VERTICAL, 3));
        recyclerView.setAdapter(productDaoAdapter);

        name = findViewById(R.id.textView2);
        name.setText(MyPreferences.getString("USER_NAME", ""));
        findViewById(R.id.logout).setOnClickListener(v -> {
            MyPreferences.saveBoolean(MyPreferences.PREF_IS_LOGIN, false);
            MyPreferences.saveString("USER_ID", "-1");
            finish();
        });

        List<ProductDbObj> pList = DatabaseHelper.getAppDatabase(this).getUserRepoJoinDao().
                getProductsOfUsers(Integer.valueOf(MyPreferences.getString("USER_ID", "-1")));

        productDbObjs.addAll(pList);

    }
}
