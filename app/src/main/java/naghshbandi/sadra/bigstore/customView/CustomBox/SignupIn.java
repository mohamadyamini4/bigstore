package naghshbandi.sadra.bigstore.customView.CustomBox;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.crashlytics.android.answers.Answers;

import java.util.List;

import io.fabric.sdk.android.Fabric;
import naghshbandi.sadra.bigstore.ProfileActivity;
import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.database.DatabaseHelper;
import naghshbandi.sadra.bigstore.objects.UserObj;
import naghshbandi.sadra.bigstore.utility.MyPreferences;

public class SignupIn extends Dialog {

    private TextView register, title;
    private EditText userName, password;
    private Button submit;
    private boolean action = true;
    private boolean allowToLogin;
    private Context context;
    private String wallet="0";


    public SignupIn(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.signup_in_layout);
        init();
    }

    private void init() {
        register = findViewById(R.id.registertxt);
        userName = findViewById(R.id.input_user);
        password = findViewById(R.id.pass);
        submit = findViewById(R.id.submit);
        title = findViewById(R.id.title);
        register.setOnClickListener(v -> {
            action = !action;
            if (action) {
                register.setText("هنوز ثبت نام نکرده ام");
                submit.setText("ورود");
                title.setText("ورود");
            } else {
                register.setText("میخواهم وارد شوم");
                submit.setText("ثبت نام");
                title.setText("ثبت نام");
            }
        });
        submit.setOnClickListener(v -> {
            if (userName.getText().toString().length() < 1 &&
                    password.getText().toString().length() < 1) {
                Toast.makeText(context, "لطفا یوزر نیم و پسورد وارد کنید", Toast.LENGTH_LONG)
                        .show();
            } else if (action) {
                int id = 0;
                List<UserObj> users = DatabaseHelper.getAppDatabase(context).getUserDao().getUsers();
                for (UserObj user : users) {
                    if (user.userName.equals(userName.getText().toString()) &&
                            user.password.equals(password.getText().toString())) {
                        allowToLogin = true;
                        id = user.id;
                        wallet = user.wallet;
                    }
                }
                if (allowToLogin) {
                    MyPreferences.saveBoolean(MyPreferences.PREF_IS_LOGIN, true);
                    MyPreferences.saveString("USER_ID", String.valueOf(id));
                    MyPreferences.saveString("USER_NAME", userName.getText().toString());
                    MyPreferences.saveString(MyPreferences.PREF_WALLET,wallet);
                    Intent intent = new Intent(context, ProfileActivity.class);
                    context.startActivity(intent);
                    dismiss();
                } else {
                    Toast.makeText(context, "نام کاربری و یا رمز عبور اشتباه است", Toast.LENGTH_LONG)
                            .show();
                }
            } else {
                UserObj userObj = new UserObj(userName.getText().toString(), password.getText().toString(),"0");
                long l = DatabaseHelper.getAppDatabase(context).getUserDao().addUser(userObj);
                MyPreferences.saveBoolean(MyPreferences.PREF_IS_LOGIN, true);
                MyPreferences.saveString("USER_ID", String.valueOf(l));
                MyPreferences.saveString("USER_NAME", userName.getText().toString());
                Intent intent = new Intent(context, ProfileActivity.class);
                context.startActivity(intent);
                dismiss();
            }
        });
    }
}
