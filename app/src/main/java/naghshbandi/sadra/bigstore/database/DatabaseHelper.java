package naghshbandi.sadra.bigstore.database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import naghshbandi.sadra.bigstore.objects.ProductDbObj;
import naghshbandi.sadra.bigstore.objects.ProductUserJoin;
import naghshbandi.sadra.bigstore.objects.UserObj;

@Database(entities = {ProductDbObj.class, UserObj.class, ProductUserJoin.class}, version = 2)
public abstract class DatabaseHelper extends RoomDatabase {
    private static DatabaseHelper INSTANCE;


    public static DatabaseHelper getAppDatabase(Context context) {
        if (INSTANCE == null) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(), DatabaseHelper.class, "big-store-database")
                            .allowMainThreadQueries()
                            .build();
        }
        return INSTANCE;
    }
    public abstract ProductDao getRepoDao();
    public abstract UserDao getUserDao();
    public abstract ProductUserJoinDao getUserRepoJoinDao();

    public static void destroyInstance() {
        INSTANCE = null;
    }

}
