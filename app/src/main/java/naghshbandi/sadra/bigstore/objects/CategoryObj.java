package naghshbandi.sadra.bigstore.objects;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CategoryObj {

    @Expose
    @SerializedName("id")
    private int id;
    @Expose
    @SerializedName("name")
    private String title;
    @SerializedName("type")
    private ProductType productType = ProductType.MUSIC;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }
}
