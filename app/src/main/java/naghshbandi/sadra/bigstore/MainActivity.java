package naghshbandi.sadra.bigstore;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.CoordinatorLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableStringBuilder;
import android.util.Log;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;

import com.crashlytics.android.answers.Answers;
import com.crashlytics.android.answers.CustomEvent;

import naghshbandi.sadra.bigstore.customView.CustomBox.SignupIn;
import naghshbandi.sadra.bigstore.fragments.CategoryFragment;
import naghshbandi.sadra.bigstore.fragments.IndexFragment;
import naghshbandi.sadra.bigstore.fragments.SearchFragment;
import naghshbandi.sadra.bigstore.utility.BottomNavigationBehavior;
import naghshbandi.sadra.bigstore.utility.CustomTypefaceSpan;
import naghshbandi.sadra.bigstore.utility.Utils;
import naghshbandi.sadra.bigstore.utility.ViewPagerAdapter;

import static naghshbandi.sadra.bigstore.utility.MyPreferences.PREF_IS_LOGIN;
import static naghshbandi.sadra.bigstore.utility.MyPreferences.getBoolean;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    private BottomNavigationView bottomNavigationView;
    private ViewPagerAdapter adapter;
    private ViewPager viewPager;
    private ImageView profileBtn;
    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            switch (item.getItemId()) {
                case R.id.homeTab:
                    viewPager.setCurrentItem(0);
                    break;
                case R.id.categoryTab:
                    viewPager.setCurrentItem(1);
                    break;
                case R.id.searchTab:
                    viewPager.setCurrentItem(3);
                    break;

            }
            return true;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initComponents();
    }

    private void initComponents() {
        Answers.getInstance().logCustom(new CustomEvent("OpenApp"));
        Log.i(TAG, "initComponents: send event");
        bottomNavigationView = findViewById(R.id.navigation);
        bottomNavigationView.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        CoordinatorLayout.LayoutParams layoutParams = (CoordinatorLayout.LayoutParams)
                bottomNavigationView.getLayoutParams();
        layoutParams.setBehavior(new BottomNavigationBehavior());
        bottomNavigationView.getMenu().findItem(R.id.homeTab).setChecked(true);
        viewPager = findViewById(R.id.view_pager);
        useCustomFont();
        setupViewPager();
        profileBtn = findViewById(R.id.profileImage);
        profileBtn.setOnClickListener(v -> {
            if (!getBoolean(PREF_IS_LOGIN, false)) {
                SignupIn s = new SignupIn(MainActivity.this);
                s.show();
                Window window = s.getWindow();
                int width = Utils.screenWidth(MainActivity.this) - Utils.dpToPx(25,
                        MainActivity.this);
                assert window != null;
                window.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT);
            } else {
                startActivity(new Intent(MainActivity.this, ProfileActivity.class));
            }
        });

    }

    private void setupViewPager() {
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(IndexFragment.getInstance());
        adapter.addFragment(CategoryFragment.newInstance());
        adapter.addFragment(SearchFragment.newInstance());
        viewPager.setAdapter(adapter);
        adapter.notifyDataSetChanged();
    }

    public void useCustomFont() {
        Typeface mTypeface = Typeface.createFromAsset(getAssets(), "IRANSans(FaNum).ttf");
        CustomTypefaceSpan typefaceSpan = new CustomTypefaceSpan("", mTypeface);
        for (int i = 0; i < bottomNavigationView.getMenu().size(); i++) {
            MenuItem menuItem = bottomNavigationView.getMenu().getItem(i);
            SpannableStringBuilder spannableTitle = new SpannableStringBuilder(menuItem.getTitle());
            spannableTitle.setSpan(typefaceSpan, 0, spannableTitle.length(), 0);
            menuItem.setTitle(spannableTitle);
        }
    }

}
