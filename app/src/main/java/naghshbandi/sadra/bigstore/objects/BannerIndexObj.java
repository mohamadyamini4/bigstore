package naghshbandi.sadra.bigstore.objects;

import com.google.gson.annotations.SerializedName;

public class BannerIndexObj {
    @SerializedName("id")
    private int id;
    @SerializedName("image")
    private String image = "";
    @SerializedName("title")
    private String title = "رضا صادقی - زندگی کن";
    @SerializedName("type")
    private ProductType productType = ProductType.MUSIC;

    public int getId() {
        return id;
    }

    public String getImage() {
        return image;
    }

    public String getTitle() {
        return title;
    }

    public ProductType getProductType() {
        return productType;
    }
}
