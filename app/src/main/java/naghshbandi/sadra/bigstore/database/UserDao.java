package naghshbandi.sadra.bigstore.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import naghshbandi.sadra.bigstore.objects.UserObj;

@Dao
public interface UserDao {
    @Query("SELECT * FROM T_USER")
    List<UserObj> getUsers();

    @Insert
    long addUser(UserObj userObj);


}
