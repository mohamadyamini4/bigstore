package naghshbandi.sadra.bigstore.network;

import java.util.List;

import naghshbandi.sadra.bigstore.objects.CategoryObj;
import naghshbandi.sadra.bigstore.objects.IndexObj;
import naghshbandi.sadra.bigstore.objects.ProductDetailObj;
import naghshbandi.sadra.bigstore.objects.ProductObj;
import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiInterface {

    @GET("cat.json")
    Call<List<CategoryObj>> getCategories();

    @GET("prod.json")
    Call<List<ProductObj>> getProducts();

    @GET("prodD.json")
    Call<List<ProductDetailObj>> getProductDetails();

    @GET("index.json")
    Call<List<IndexObj>> getIndex();

}