package naghshbandi.sadra.bigstore.utility;

import java.io.Serializable;

public interface FinishMusic extends Serializable {
    void onFinish();
}