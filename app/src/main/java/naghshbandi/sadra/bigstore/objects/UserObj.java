package naghshbandi.sadra.bigstore.objects;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "t_user")
public class UserObj {
    @PrimaryKey(autoGenerate = true)
    public int id;

    @ColumnInfo(name = "userName")
    public String userName;

    @ColumnInfo(name = "password")
    public String password;

    @ColumnInfo(name = "wallet")
    public String wallet;

    public UserObj(String userName, String password, String wallet) {
        this.userName = userName;
        this.password = password;
        this.wallet = wallet;
    }
}
