package naghshbandi.sadra.bigstore.database;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;

import java.util.List;

import naghshbandi.sadra.bigstore.objects.ProductDbObj;
import naghshbandi.sadra.bigstore.objects.ProductUserJoin;

@Dao
public interface ProductUserJoinDao {
    @Insert
    void insert(ProductUserJoin productUserJoin);

    @Query("SELECT * FROM t_product INNER JOIN t_product_user_join ON t_product.id=t_product_user_join.repoId" +
            " where t_product_user_join.userId=:userId")
    List<ProductDbObj> getProductsOfUsers(final int userId);

    @Query("SELECT * FROM T_PRODUCT_USER_JOIN")
    List<ProductUserJoin> getList();
}
