package naghshbandi.sadra.bigstore.adapters;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.List;

import naghshbandi.sadra.bigstore.AppLoader;
import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.objects.BannerIndexObj;
import naghshbandi.sadra.bigstore.utility.AdapterClickHandler;

public class BannerIndexAdapter extends RecyclerView.Adapter<BannerIndexAdapter.ViewHolder> {
    private List<BannerIndexObj> bannerIndexObjs;
    private AdapterClickHandler bannerClickHandler;


    BannerIndexAdapter(List<BannerIndexObj> bannerIndexObjs) {
        this.bannerIndexObjs = bannerIndexObjs;
    }

    void setBannerClickHandler(AdapterClickHandler bannerClickHandler) {
        this.bannerClickHandler = bannerClickHandler;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_banner_index,
                viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder v, int i) {
        BannerIndexObj bannerIndexObj = bannerIndexObjs.get(i);
        Glide.with(AppLoader.getAppContext())
                .load(bannerIndexObj.getImage())
                .placeholder(R.drawable.banner_one)
                .into(v.image);
        v.title.setText(bannerIndexObj.getTitle());
    }

    @Override
    public int getItemCount() {
        return bannerIndexObjs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ImageView image;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvTitleIndexBanner);
            image = itemView.findViewById(R.id.ivIndexBanner);
            image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (bannerClickHandler != null)
                        bannerClickHandler.onClick(getAdapterPosition());
                }
            });
        }
    }
}
