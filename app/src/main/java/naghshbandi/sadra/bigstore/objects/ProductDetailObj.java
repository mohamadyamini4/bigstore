package naghshbandi.sadra.bigstore.objects;

import com.google.gson.annotations.SerializedName;

public class ProductDetailObj {

    @SerializedName("id")
    private int id;
    @SerializedName("title")
    private String title;
    @SerializedName("title2")
    private String title2;
    @SerializedName("sampleFile")
    private String sampleMusic;
    @SerializedName("image")
    private String thumbnail;
    @SerializedName("description")
    private String description;
    @SerializedName("price")
    private String price;
    @SerializedName("type")
    private ProductType productType = ProductType.MUSIC;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getTitle2() {
        return title2;
    }

    public String getSampleMusic() {
        return sampleMusic;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public String getDescription() {
        return description;
    }

    public String getPrice() {
        return price;
    }

    public ProductType getProductType() {
        return productType;
    }
}
