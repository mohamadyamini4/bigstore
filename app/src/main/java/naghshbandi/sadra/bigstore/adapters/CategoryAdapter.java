package naghshbandi.sadra.bigstore.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.objects.CategoryObj;
import naghshbandi.sadra.bigstore.utility.AdapterClickHandler;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.ViewHolder> {
    private List<CategoryObj> categoryObjs;
    private Context context;
    private AdapterClickHandler adapterClickHandler;

    public CategoryAdapter(List<CategoryObj> categoryObjs, Context context, AdapterClickHandler adapterClickHandler) {
        this.categoryObjs = categoryObjs;
        this.context = context;
        this.adapterClickHandler = adapterClickHandler;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_category,
                viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        CategoryObj categoryObj = categoryObjs.get(i);
        viewHolder.title.setText(categoryObj.getTitle());
    }

    @Override
    public int getItemCount() {
        return categoryObjs.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        ConstraintLayout constraintLayout;

        ViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.tvCategoryTitle);
            constraintLayout = itemView.findViewById(R.id.clContainer);
            constraintLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (adapterClickHandler != null) {
                        adapterClickHandler.onClick(getAdapterPosition());
                    }
                }
            });

        }
    }
}
