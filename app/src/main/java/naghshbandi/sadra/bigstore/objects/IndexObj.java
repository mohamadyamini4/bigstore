package naghshbandi.sadra.bigstore.objects;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import naghshbandi.sadra.bigstore.IndexViewType;

public class IndexObj {
    @SerializedName("title")
    private String title;
    @SerializedName("indexViewType")
    private IndexViewType indexViewType;
    @SerializedName("bannerList")
    private List<BannerIndexObj> bannerIndexObjs;
    @SerializedName("productList")
    private List<ProductIndexObj> productIndexObjs;

    public IndexObj(String title, IndexViewType indexViewType,
                    List<BannerIndexObj> bannerIndexObjs,
                    List<ProductIndexObj> productIndexObjs) {
        this.title = title;
        this.indexViewType = indexViewType;
        this.bannerIndexObjs = bannerIndexObjs;
        this.productIndexObjs = productIndexObjs;
    }

    public String getTitle() {
        return title;
    }

    public IndexViewType getIndexViewType() {
        return indexViewType;
    }

    public List<BannerIndexObj> getBannerIndexObjs() {
        return bannerIndexObjs;
    }

    public List<ProductIndexObj> getProductIndexObjs() {
        return productIndexObjs;
    }
}
