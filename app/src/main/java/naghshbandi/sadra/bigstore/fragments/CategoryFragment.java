package naghshbandi.sadra.bigstore.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import naghshbandi.sadra.bigstore.EachCategoryActivity;
import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.adapters.CategoryAdapter;
import naghshbandi.sadra.bigstore.network.ApiClient;
import naghshbandi.sadra.bigstore.network.ApiInterface;
import naghshbandi.sadra.bigstore.objects.CategoryObj;
import naghshbandi.sadra.bigstore.utility.MyDividerItemDecoration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryFragment extends Fragment {
    private RecyclerView recyclerView;
    private CategoryAdapter categoryAdapter;
    private List<CategoryObj> categoryObjs = new ArrayList<>();
    private boolean hasLoadedOnce;


    public CategoryFragment() {
    }


    public static CategoryFragment newInstance() {
        return new CategoryFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View inflate = inflater.inflate(R.layout.fragment_category, container, false);
        recyclerView = inflate.findViewById(R.id.recView);
        categoryAdapter = new CategoryAdapter(categoryObjs, getActivity(), position -> {
            Intent i = new Intent(getActivity(), EachCategoryActivity.class);
            i.putExtra("id", categoryObjs.get(position).getId());
            startActivity(i);
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(getActivity(),
                DividerItemDecoration.VERTICAL, 3));
        recyclerView.setAdapter(categoryAdapter);

        return inflate;
    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                setup();
                hasLoadedOnce = true;
            }

        }
    }

    private void setup() {
        ApiInterface ai = ApiClient.getClient(getActivity()).create(ApiInterface.class);

        ai.getCategories().enqueue(new Callback<List<CategoryObj>>() {
            @Override
            public void onResponse(Call<List<CategoryObj>> call, Response<List<CategoryObj>> response) {
                categoryObjs.addAll(response.body());
                categoryAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<CategoryObj>> call, Throwable t) {

            }
        });

    }
}
