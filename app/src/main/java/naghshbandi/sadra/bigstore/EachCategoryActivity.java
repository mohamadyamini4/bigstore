package naghshbandi.sadra.bigstore;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import naghshbandi.sadra.bigstore.adapters.ProductAdapter;
import naghshbandi.sadra.bigstore.network.ApiClient;
import naghshbandi.sadra.bigstore.network.ApiInterface;
import naghshbandi.sadra.bigstore.objects.ProductObj;
import naghshbandi.sadra.bigstore.utility.MyDividerItemDecoration;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class EachCategoryActivity extends AppCompatActivity {

    private RecyclerView recyclerView;
    private List<ProductObj> productObjs = new ArrayList<>();
    private ProductAdapter productAdapter;
    private int gfpa = -1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_each_category);
        try {
            gfpa = getIntent().getExtras().getInt("id");
        } catch (Exception ignored) {

        }

        recyclerView = findViewById(R.id.recView);
        productAdapter = new ProductAdapter(productObjs, this, position -> {
            Intent i = new Intent(EachCategoryActivity.this, eachProductActivity.class);
            i.putExtra("id", productObjs.get(position).getId());
            startActivity(i);

        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.addItemDecoration(new MyDividerItemDecoration(this,
                DividerItemDecoration.VERTICAL, 3));
        recyclerView.setAdapter(productAdapter);

        fetchList();
    }

    private void fetchList() {
        ApiInterface ai = ApiClient.getClient(this).create(ApiInterface.class);
        ai.getProducts().enqueue(new Callback<List<ProductObj>>() {
            @Override
            public void onResponse(Call<List<ProductObj>> call, Response<List<ProductObj>> response) {
                for (ProductObj productObj : response.body()) {
                    if (productObj.getCid() == gfpa) {
                        productObjs.add(productObj);
                    }
                }
                productAdapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<List<ProductObj>> call, Throwable t) {

            }
        });
    }
}
