package naghshbandi.sadra.bigstore.objects;

import com.google.gson.annotations.SerializedName;

public class ProductObj {

    @SerializedName("id")
    private int id;

    @SerializedName("name")
    private String title;

    @SerializedName("price")
    private String price;

    @SerializedName("image")
    private String thumbnial;

    @SerializedName("cid")
    private int cid;

    @SerializedName("type")
    private ProductType productType = ProductType.MUSIC;

    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getPrice() {
        return price;
    }

    public String getThumbnail() {
        return thumbnial;
    }

    public int getCid() {
        return cid;
    }
}

