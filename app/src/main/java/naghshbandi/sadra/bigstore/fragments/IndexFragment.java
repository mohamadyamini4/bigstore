package naghshbandi.sadra.bigstore.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.adapters.MainIndexAdapter;
import naghshbandi.sadra.bigstore.network.ApiClient;
import naghshbandi.sadra.bigstore.network.ApiInterface;
import naghshbandi.sadra.bigstore.objects.IndexObj;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class IndexFragment extends Fragment {
    private RecyclerView recyclerView;
    private List<IndexObj> indexObjs = new ArrayList<>();
    private MainIndexAdapter mainIndexAdapter;
    private boolean hasLoadedOnce;

    public IndexFragment() {
    }

    public static IndexFragment getInstance() {
        return new IndexFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_index, container, false);
        init(view);
        return view;
    }

    private void init(View v) {
        recyclerView = v.findViewById(R.id.recView);
        mainIndexAdapter = new MainIndexAdapter(indexObjs, getActivity());
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mainIndexAdapter);
        recyclerView.setNestedScrollingEnabled(false);
        recyclerView.setHasFixedSize(true);
        if (!hasLoadedOnce)
        prepareData();
    }

    private void prepareData() {
        hasLoadedOnce = true;
        ApiClient.getClient(getActivity()).create(ApiInterface.class).getIndex().
                enqueue(new Callback<List<IndexObj>>() {
                    @Override
                    public void onResponse(Call<List<IndexObj>> call, Response<List<IndexObj>> response) {
                        indexObjs.addAll(response.body());
                        mainIndexAdapter.notifyDataSetChanged();
                    }

                    @Override
                    public void onFailure(Call<List<IndexObj>> call, Throwable t) {

                    }
                });
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(true);
        if (this.isVisible()) {
            if (isVisibleToUser && !hasLoadedOnce) {
                hasLoadedOnce = true;
                prepareData();
            }

        }
    }
}
