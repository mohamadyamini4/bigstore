package naghshbandi.sadra.bigstore;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.IBinder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;

import java.util.List;

import naghshbandi.sadra.bigstore.database.DatabaseHelper;
import naghshbandi.sadra.bigstore.network.ApiClient;
import naghshbandi.sadra.bigstore.network.ApiInterface;
import naghshbandi.sadra.bigstore.objects.ProductDbObj;
import naghshbandi.sadra.bigstore.objects.ProductDetailObj;
import naghshbandi.sadra.bigstore.objects.ProductUserJoin;
import naghshbandi.sadra.bigstore.player.MediaPlayerService;
import naghshbandi.sadra.bigstore.utility.FinishMusic;
import naghshbandi.sadra.bigstore.utility.MyPreferences;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static naghshbandi.sadra.bigstore.utility.MyPreferences.PREF_IS_LOGIN;
import static naghshbandi.sadra.bigstore.utility.MyPreferences.PREF_WALLET;
import static naghshbandi.sadra.bigstore.utility.MyPreferences.getBoolean;

public class eachProductActivity extends AppCompatActivity implements FinishMusic {

    boolean serviceBound = false;
    PlayingStatus playingStatus = PlayingStatus.NOT_PLAY;
    private int gfpa = -1;
    private ImageView image;
    private TextView title, title2, description, price;
    private TextView tvBuy;
    private ImageView ivPlay;
    private ProductDetailObj pAll;
    private MediaPlayerService player;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            MediaPlayerService.LocalBinder binder = (MediaPlayerService.LocalBinder) service;
            player = binder.getService();
            serviceBound = true;
            player.setFinishMusic(eachProductActivity.this);
            Toast.makeText(eachProductActivity.this, "Service Bound", Toast.LENGTH_SHORT).show();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            serviceBound = false;
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_each_product_activiy);
        try {
            gfpa = getIntent().getExtras().getInt("id");
        } catch (Exception ignored) {
        }

        image = findViewById(R.id.ivProductThumbnail);
        title = findViewById(R.id.tvProductTitle);
        title2 = findViewById(R.id.tvProductTitle2);
        description = findViewById(R.id.tvDescription);
        price = findViewById(R.id.tvPrice);
        tvBuy = findViewById(R.id.tvBuyProduct);
        ivPlay = findViewById(R.id.ivPlay);
        if (getBoolean(PREF_IS_LOGIN, false)) {
            List<ProductDbObj> prod = DatabaseHelper.getAppDatabase(this).getUserRepoJoinDao().
                    getProductsOfUsers(Integer.valueOf(MyPreferences.getString("USER_ID", "-1")));

            for (ProductDbObj productDbObj : prod) {
                if (productDbObj.getId() == gfpa) {
                    tvBuy.setVisibility(View.INVISIBLE);
                }
            }
        }

        tvBuy.setOnClickListener(v -> {

            if (getBoolean(PREF_IS_LOGIN, false)) {
                String curWallet = MyPreferences.getString(PREF_WALLET, "0");
                Integer currWalletInt = Integer.valueOf(curWallet);
                if (currWalletInt < Integer.valueOf(pAll.getPrice())) {
                    Toast.makeText(eachProductActivity.this, "اعتبار شما کمتر است", Toast.LENGTH_SHORT).show();
                    return;
                }
                ProductDbObj productDbObj = new ProductDbObj(pAll.getTitle(), pAll.getPrice(),
                        pAll.getThumbnail(),pAll.getProductType().name());
                long l = DatabaseHelper.getAppDatabase(this).getRepoDao().
                        insertProduct(productDbObj);

                DatabaseHelper.getAppDatabase(this).getUserRepoJoinDao().
                        insert(new ProductUserJoin(Integer.valueOf(MyPreferences.getString("USER_ID", "-1")),
                                Integer.valueOf(String.valueOf(l))));
                Toast.makeText(this, "خرید انجام شد", Toast.LENGTH_LONG).show();
            }else {
                Toast.makeText(this, "ابتدا ثبت نام کنید", Toast.LENGTH_SHORT).show();
            }
        });

        fetchData();

        ivPlay.setOnClickListener(v -> playAudio(pAll.getSampleMusic()));
    }

    private void playAudio(String media) {

        if (playingStatus == PlayingStatus.NOT_PLAY) {
            if (!serviceBound) {
                Intent playerIntent = new Intent(this, MediaPlayerService.class);
                playerIntent.putExtra("media", media);
                bindService(playerIntent, serviceConnection, Context.BIND_AUTO_CREATE);
                startService(playerIntent);
                playingStatus = PlayingStatus.PLAY;
                Glide.with(eachProductActivity.this).load(R.drawable.ic_pause).into(ivPlay);
            }
        } else if (playingStatus == PlayingStatus.PLAY) {
            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            playerIntent.putExtra("pause", true);
            startService(playerIntent);
            playingStatus = PlayingStatus.PAUSE;
            Glide.with(eachProductActivity.this).load(R.drawable.ic_play).into(ivPlay);
        } else if (playingStatus == PlayingStatus.PAUSE) {
            Intent playerIntent = new Intent(this, MediaPlayerService.class);
            playerIntent.putExtra("play", true);
            startService(playerIntent);
            playingStatus = PlayingStatus.PLAY;
            Glide.with(eachProductActivity.this).load(R.drawable.ic_pause).into(ivPlay);

        }
    }

    private void fetchData() {
        ApiClient.getClient(this).create(ApiInterface.class).
                getProductDetails().enqueue(new Callback<List<ProductDetailObj>>() {
            @Override
            public void onResponse(Call<List<ProductDetailObj>> call,
                                   Response<List<ProductDetailObj>> response) {

                for (ProductDetailObj productDetailObj : response.body()) {
                    if (productDetailObj.getId() == gfpa) {
                        showData(productDetailObj);
                        break;
                    }
                }
            }

            @Override
            public void onFailure(Call<List<ProductDetailObj>> call, Throwable t) {

            }
        });
    }

    private void showData(ProductDetailObj productDetailObj) {
        pAll = productDetailObj;
        Glide.with(this).load(pAll.getThumbnail()).into(image);
        title.setText(pAll.getTitle());
        title2.setText(pAll.getTitle2());
        description.setText(pAll.getDescription());
        price.setText(pAll.getPrice() + "  تومان  ");
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        savedInstanceState.putBoolean("ServiceState", serviceBound);
        super.onSaveInstanceState(savedInstanceState);
    }

    @Override
    public void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        serviceBound = savedInstanceState.getBoolean("ServiceState");
    }

    @Override
    public void onBackPressed() {
        if (serviceBound) {
            unbindService(serviceConnection);
            player.stopSelf();
        }
        super.onBackPressed();
    }

    @Override
    public void onFinish() {
        if (serviceBound) {
            unbindService(serviceConnection);
            player.stopSelf();
            serviceBound = false;
            playingStatus = PlayingStatus.NOT_PLAY;
            Glide.with(eachProductActivity.this).load(R.drawable.ic_play).into(ivPlay);
        }

    }


    public enum PlayingStatus {
        NOT_PLAY,
        PLAY,
        PAUSE,
        FINISH
    }
}
