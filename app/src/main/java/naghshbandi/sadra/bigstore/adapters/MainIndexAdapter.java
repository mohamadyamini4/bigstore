package naghshbandi.sadra.bigstore.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.PagerSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import naghshbandi.sadra.bigstore.AppLoader;
import naghshbandi.sadra.bigstore.R;
import naghshbandi.sadra.bigstore.eachProductActivity;
import naghshbandi.sadra.bigstore.objects.BannerIndexObj;
import naghshbandi.sadra.bigstore.objects.IndexObj;
import naghshbandi.sadra.bigstore.objects.ProductIndexObj;
import naghshbandi.sadra.bigstore.objects.ProductType;


public class MainIndexAdapter extends RecyclerView.Adapter {
    private List<IndexObj> indexObjs;
    private Context context;

    public MainIndexAdapter(List<IndexObj> indexObjs, Context context) {
        this.indexObjs = indexObjs;
        this.context = context;
    }

    public void setIndexObjs(List<IndexObj> indexObjs) {
        this.indexObjs = indexObjs;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder vh;
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_main_index,
                viewGroup, false);
        vh = new IndexViewHolder(view);
        return vh;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        IndexObj indexObj = indexObjs.get(i);
        switch (indexObj.getIndexViewType()) {
            case BANNER:
                bannerSetup(indexObj, ((IndexViewHolder) viewHolder));
                break;
            case PRODUCT:
                productSetup(indexObj, ((IndexViewHolder) viewHolder));
                break;
        }
        ((IndexViewHolder) viewHolder).title.setText(indexObj.getTitle());
    }

    private void productSetup(IndexObj indexObj, IndexViewHolder viewHolder) {
        List<ProductIndexObj> products = indexObj.getProductIndexObjs();
        if (products != null && !products.isEmpty()) {
            ProductIndexAdapter productIndexAdapter = new ProductIndexAdapter(products);
            productIndexAdapter.setAdapterClickHandler(position -> {
                Intent i = new Intent(context, eachProductActivity.class);
                i.putExtra("id", products.get(position).getId());
                context.startActivity(i);
            });
            horizontalRecycler(productIndexAdapter, viewHolder);
            viewHolder.title.setText(indexObj.getTitle());
        }
    }

    private void bannerSetup(IndexObj indexObj, IndexViewHolder viewHolder) {
        final List<BannerIndexObj> banners = indexObj.getBannerIndexObjs();
        if (banners != null && !banners.isEmpty()) {
            BannerIndexAdapter bannerIndexAdapter = new BannerIndexAdapter(banners);
            bannerRecycler(bannerIndexAdapter, viewHolder);
            bannerIndexAdapter.setBannerClickHandler(position -> bannerClick(banners.get(position)));
            viewHolder.title.setText(indexObj.getTitle());
        }

    }

    private void bannerClick(BannerIndexObj bannerObj) {
        if (ProductType.MUSIC.equals(bannerObj.getProductType())) {
            Intent i = new Intent(context, eachProductActivity.class);
            i.putExtra("id", bannerObj.getId());
            context.startActivity(i);
        }
    }


    private void bannerRecycler(BannerIndexAdapter bannerIndexAdapter, IndexViewHolder viewHolder) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(AppLoader.getAppContext(),
                LinearLayoutManager.HORIZONTAL, false);
        viewHolder.recyclerView.setLayoutManager(layoutManager);
        viewHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        viewHolder.recyclerView.setAdapter(bannerIndexAdapter);
        SnapHelper snapHelper = new PagerSnapHelper();
        viewHolder.recyclerView.setOnFlingListener(null);
        snapHelper.attachToRecyclerView(viewHolder.recyclerView);
        viewHolder.recyclerView.setNestedScrollingEnabled(false);
        viewHolder.recyclerView.setHasFixedSize(true);
    }

    private void horizontalRecycler(RecyclerView.Adapter bannerIndexAdapter, IndexViewHolder viewHolder) {
        LinearLayoutManager layoutManager
                = new LinearLayoutManager(AppLoader.getAppContext(), LinearLayoutManager.HORIZONTAL, true);
        viewHolder.recyclerView.setLayoutManager(layoutManager);
        viewHolder.recyclerView.setItemAnimator(new DefaultItemAnimator());
        viewHolder.recyclerView.setAdapter(bannerIndexAdapter);
        viewHolder.recyclerView.setNestedScrollingEnabled(false);
        viewHolder.recyclerView.setHasFixedSize(true);
    }

    @Override
    public int getItemCount() {
        return indexObjs.size();
    }

    class IndexViewHolder extends RecyclerView.ViewHolder {
        TextView title;
        RecyclerView recyclerView;

        IndexViewHolder(View v) {
            super(v);
            title = v.findViewById(R.id.tvMainTitleIndex);
            recyclerView = v.findViewById(R.id.recView);
        }
    }

}