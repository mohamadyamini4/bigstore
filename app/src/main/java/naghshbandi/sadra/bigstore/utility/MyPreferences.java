package naghshbandi.sadra.bigstore.utility;

import android.content.Context;
import android.content.SharedPreferences;

import naghshbandi.sadra.bigstore.AppLoader;

public class MyPreferences {

    public static final String PREF_IS_LOGIN = "PREF_IS_LOGIN";
    public static final String PREF_USER_ID = "PREF_USER_ID";
    public static final String PREF_RATE_BOX = "PREF_RATE_BOX";
    private static final String SHARED_PREFERENCES_NAME = "BIG_STORE_APP";
    public static final String PREF_WALLET="PREF_WALLET";

    private static SharedPreferences getPrefs() {
        return AppLoader.getAppContext().getSharedPreferences(SHARED_PREFERENCES_NAME, Context.MODE_PRIVATE);
    }

    public static void saveString(String key, String value) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getString(String key, String value) {
        return getPrefs().getString(key, value);
    }

    public static boolean getBoolean(String key, boolean defaultValue) {
        return getPrefs().getBoolean(key, defaultValue);
    }

    public static void saveBoolean(String key, boolean value) {
        SharedPreferences.Editor editor = getPrefs().edit();
        editor.putBoolean(key, value);
        editor.apply();
    }
}
